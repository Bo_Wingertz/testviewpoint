package vp.compliance.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {

	//create object for properties class
	Properties pro;
	
	//create constructor to load properties file
	public ReadConfig()
	{
		File src= new File("./Configuration/config.properties");
	
		try {
		FileInputStream fis = new FileInputStream(src);
		pro = new Properties();
		pro.load(fis);
	}
	
	catch(Exception e)
	{
		System.out.println("Exception is " + e.getMessage());
	}
	}
	
	/*create methods to read every variable from properties file.Get URLs*/
public String getVPintranetURL()
{
	String IntranetURL=pro.getProperty("IntranetURL");
	return IntranetURL;
}

public String getExtranetstageURL()
{
	String ExtranetstageURL=pro.getProperty("ExtranetstageURL");
	return ExtranetstageURL;
}

/*Get browser paths*/

public String getChromePath()
{
	String chromepath=pro.getProperty("chromepath");
	return chromepath;
}

public String getIEPath()
{
	String iepath=pro.getProperty("iepath");
	return iepath;
}

public String getEdgePath()
{
	String edgepath=pro.getProperty("edgepath");
	return edgepath;
}

public String getFirefoxPath()
{
	String firefoxpath=pro.getProperty("firefoxpath");
	return firefoxpath;
}

}
