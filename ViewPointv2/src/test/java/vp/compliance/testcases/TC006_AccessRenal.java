package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;

public class TC006_AccessRenal extends BaseClass{
	@Test
	public void AccessOncology() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickRenal();
		captureScreen(driver,"RenalPortal");
		logger.info("Accessed Renal portal");
		
		boolean Page=driver.getPageSource().contains("Renal");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on Renal portal");
		}
		else
		{
			captureScreen(driver,"RenalPortal");
			Assert.assertTrue(false);
			logger.info("Did not land on Renal portal");
		}
	
	}
		
}