package vp.compliance.testcases;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;


import vp.compliance.utilities.ReadConfig;

public class BaseClass {

	//create an object for utility file
ReadConfig readconfig=new ReadConfig();
	
	
	public String IntranetURL= readconfig.getVPintranetURL(); 
	public String ExtranetURL= readconfig.getExtranetstageURL(); 
	
	public static WebDriver driver;
	public static Logger logger;
	
	//passing browser as a parameter to run tests on desired browser
	@Parameters("browser")
	@BeforeClass
	public void setup(String br)
	{
		
		if (br.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", readconfig.getChromePath());
			driver=new ChromeDriver();	
		}
		
		else if (br.equalsIgnoreCase("ie"))
		{
			System.setProperty("webdriver.ie.driver", readconfig.getIEPath());
			driver=new InternetExplorerDriver();	
		}
		
		else if (br.equalsIgnoreCase("edge"))
		{
			System.setProperty("webdriver.edge.driver", readconfig.getEdgePath());
			driver=new EdgeDriver();	
			
			/*WebDriverManager manager = WebDriverManager.edgedriver();
			manager.config().setEdgeDriverVersion("84.0.522.49");
			manager.setup();
			EdgeOptions options = new EdgeOptions();
			driver = new EdgeDriver(options);*/
		}
		
		else if (br.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", readconfig.getFirefoxPath());
			driver=new FirefoxDriver();	
		}
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		logger=LogManager.getLogger("BaseClass.class");
		PropertyConfigurator.configure("log4j2.properties");

	}
	
	@AfterClass
	public void teardown()
	{
		driver.quit();
	}
	
	public void captureScreen(WebDriver driver,String tname) throws IOException
{
	TakesScreenshot ts = (TakesScreenshot) driver;
	File source = ts.getScreenshotAs(OutputType.FILE);
	File target = new File(System.getProperty("user.dir") + "/Screenshots/" + tname + ".png");
	FileUtils.copyFile(source, target);
	System.out.println("Screenshot taken");
}
}
