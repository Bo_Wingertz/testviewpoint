package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;

public class TC005_AccessOncology extends BaseClass{
	@Test
	public void AccessOncology() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickOncology();
		captureScreen(driver,"OncologyPortal");
		logger.info("Accessed Oncology portal");
		
		boolean Page=driver.getPageSource().contains("Oncology");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on Oncology portal");
		}
		else
		{
			captureScreen(driver,"OncologyPortal");
			Assert.assertTrue(false);
			logger.info("Did not land on Oncology portal");
		}
	
	}
		
}