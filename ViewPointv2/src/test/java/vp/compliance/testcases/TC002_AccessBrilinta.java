package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;

public class TC002_AccessBrilinta extends BaseClass{
	@Test
	public void AccessBrilinta() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickBrilinta();
		captureScreen(driver,"BrilintaPortal");
		logger.info("Accessed Brilinta portal");
		
		boolean Page=driver.getPageSource().contains("BRILINTA");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on BRILINTA portal");
		}
		else
		{
			captureScreen(driver,"BrilintaPortal");
			Assert.assertTrue(false);
			logger.info("Did not land on BRILINTA portal");
		}
	
	}
		
}

