package vp.compliance.testcases;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.LoginPage;
import vp.compliance.utilities.XLUtils;

public class TC_VPExtranetLogin extends BaseClass
{

@Test(dataProvider="LoginData")

public void ExtranetLogin(String loginid,String Pwd)
{
	LoginPage lp=new LoginPage(driver);
	lp.setEmail(loginid);
	logger.info("Entered loginid");
	lp.clickNext();
	logger.info("Landed on Passowrd page");
	lp.setPassword(Pwd);
	logger.info("Entered Password");
	lp.clickSubmit();
	logger.info("Landed on Extranet Portal list");
	
}

@DataProvider(name="LoginData")
String [][] getData() throws IOException
{
	String path=System.getProperty("user.dir")+"/src/test/java/vp/compliance/testdata/ExtranetUser.xlsx";
	int rownumber=XLUtils.getRowCount(path, "Sheet1");
	int colcount=XLUtils.getCellCount(path, "Sheet1", 1);
	String logindata[][]=new String[rownumber][colcount];
	for (int i=0;i<=rownumber;i++)
	{
		for (int j=0;j<=colcount;j++)
		{
			logindata[i][j]=XLUtils.getCellData(path, "Sheet1", i,j);
		}
	}
	return logindata;
}
}

