package vp.compliance.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Professionals {
	WebDriver ldriver;
	
	public Professionals(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
	@FindBy(linkText="Professionals")
	@CacheLookup
	WebElement ProfessionalsTab;

	//Action Method	
	public void clickProfessionals()
	{
		ProfessionalsTab.click();	
	}
	
	
	@FindBy(name="_ctl7:ctlSearchControl:cbxSearchIDType")
	@CacheLookup
	WebElement SearchIDtype;
	
	public void clickID()
	{
		SearchIDtype.click();
	}
	public void selectAZID()
	{
		Select IDtype=new Select(SearchIDtype);
		IDtype.selectByValue("AZID");
	}
	public void selectPersonID()
	{
		Select IDtype=new Select(SearchIDtype);
		IDtype.selectByValue("PERSONID");
	}
	public void selectIMSDR()
	{
		Select IDtype=new Select(SearchIDtype);
		IDtype.selectByValue("IMSDR");
	}
	public void selectNPI()
	{
		Select IDtype=new Select(SearchIDtype);
		IDtype.selectByValue("NPI");
	}
	@FindBy(id="_ctl7_ctlSearchControl_txtSearchID")
	@CacheLookup
	WebElement SearchText;
	
	public void enterID(String AZID)
	{
		SearchText.sendKeys(AZID);
	}
	@FindBy(id="_ctl7_ctlSearchControl_lnkSearch")
	@CacheLookup
	WebElement Search;
	
	public void clickSearch()
	{
		Search.click();
	}
	
	@FindBy(id="_ctl7_ctlSearchControl_lnkClear")
	@CacheLookup
	WebElement Clear;
	
	public void clickClear()
	{
		Clear.click();
	}
}
